# Discord Bot

## Install

1. ソフトウェアを揃える
  * python v3.9
  * poetry
    * [公式サイト](https://python-poetry.org)
    * [インストール方法の解説](https://python-poetry.org/docs/#installation)
2. ライブラリの入手
  ```sh
  poetry install
  ```

## run

以下のようなコマンドを実行すれば動作するはずです。

```sh
poetry run python main.py
```
